import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maga_app/app/auth/data/auth_repository.dart';
import 'package:maga_app/app/auth/ui/login_screen.dart';
import 'package:maga_app/app/auth/ui/registration_screen.dart';
import 'package:maga_app/app/auth/ui/registration_success.dart';
import 'package:maga_app/app/auth/ui/reset_passwort_screen.dart';
import 'package:maga_app/app/auth/ui/splash_screen.dart';
import 'package:maga_app/app/dashboard/data/dashboard_repository.dart';

import 'app/dashboard/ui/screens/dashboard_page.dart';

/// Handles all PAGE routing in the app. This only applies to Full Pages like "Dashboard"
class MagaRouter {
  /// all route names are defined here as static strings.
  static const String dashboardRoute = '/dashboard';
  static const String loginRoute = '/login';
  static const String resetPasswordRoute = '/reset_password';
  static const String registrationRoute = '/registration';
  static const String registrationSuccessRoute = '/registration_success';

  /// this page will be used as fallback, when there is no matching route
  static Widget defaultPage = RepositoryProvider<AuthRepository>(
    create: (_) => AuthRepository(),
    child: const SplashScreen(),
  );

  /// define navigation behavior here
  static Route<dynamic> generateRoute(RouteSettings settings) {
    late Widget page;
    switch (settings.name) {
      case dashboardRoute:
        page = RepositoryProvider<DashboardRepository>(
          create: (_) => DashboardRepository(),
          child: const DashboardPage(),
        );
        break;
      case loginRoute:
        page = const LoginScreen();
        break;
      case resetPasswordRoute:
        page = const ResetPasswordScreen();
        break;
      case registrationRoute:
        page = const RegistrationScreen();
        break;
      case registrationSuccessRoute:
        page = const RegistrationSuccessScreen();
        break;
      default:
        page = defaultPage;
    }

    return MaterialPageRoute(builder: (_) => page);
  }
}
