import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maga_app/app/auth/data/auth_repository.dart';
import 'package:maga_app/app/auth/logic/auth_bloc.dart';
import 'package:maga_app/mage_theme.dart';
import 'package:maga_app/router.dart';
import 'firebase_options.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  runApp(MagaApp());
}

class MagaApp extends StatelessWidget {
  MagaApp({super.key});

  final _authRepository = AuthRepository();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(create: (_) => _authRepository),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (_) => AuthCubit(
              authRepository: _authRepository,
            ),
          )
        ],
        child: MaterialApp(
          title: 'Maga App',
          theme: magaLightTheme,
          onGenerateRoute: MagaRouter.generateRoute,
        ),
      ),
    );
  }
}
