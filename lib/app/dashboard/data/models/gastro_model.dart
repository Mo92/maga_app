// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class GastroModel {
  // TODO(Mo): add geopoint
  // final GeoPoint location;
  final String city;
  final String imageUrl;
  final String restaurantName;
  final String postalCode;
  final String streetName;
  final String description;
  final String houseNr;
  final QueueModel queue;
  final SocialsModel socials;
  GastroModel({
    // required this.location,
    required this.city,
    required this.imageUrl,
    required this.restaurantName,
    required this.postalCode,
    required this.streetName,
    required this.description,
    required this.houseNr,
    required this.queue,
    required this.socials,
  });

  factory GastroModel.fromMap(Map<String, dynamic> map) {
    return GastroModel(
      // location: GeoPoint.fromMap(map['location'] as Map<String, dynamic>),
      city: map['city'] as String,
      imageUrl: map['imageUrl'] as String,
      restaurantName: map['restaurantName'] as String,
      postalCode: map['postalCode'] as String,
      streetName: map['streetName'] as String,
      description: map['description'] as String,
      houseNr: map['houseNr'] as String,
      queue: QueueModel.fromMap(map['queue'] as Map<String, dynamic>),
      socials: SocialsModel.fromMap(map['socials'] as Map<String, dynamic>),
    );
  }

  factory GastroModel.fromJson(String source) =>
      GastroModel.fromMap(json.decode(source) as Map<String, dynamic>);
}

class QueueModel {
  final String average;
  final int waiting;
  final bool isActive;

  QueueModel({
    required this.average,
    required this.waiting,
    required this.isActive,
  });

  QueueModel copyWith({
    String? average,
    int? waiting,
    bool? isActive,
  }) {
    return QueueModel(
      average: average ?? this.average,
      waiting: waiting ?? this.waiting,
      isActive: isActive ?? this.isActive,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'average': average,
      'waiting': waiting,
      'isActive': isActive,
    };
  }

  factory QueueModel.fromMap(Map<String, dynamic> map) {
    return QueueModel(
      average: map['average'] as String,
      waiting: map['waiting'] as int,
      isActive: map['isActive'] as bool,
    );
  }

  String toJson() => json.encode(toMap());

  factory QueueModel.fromJson(String source) =>
      QueueModel.fromMap(json.decode(source) as Map<String, dynamic>);
}

class SocialsModel {
  final String? twitter;
  final String? instagram;
  final String? website;
  final String? phone;

  SocialsModel({
    this.twitter,
    this.instagram,
    this.website,
    this.phone,
  });

  factory SocialsModel.fromMap(Map<String, dynamic> map) {
    return SocialsModel(
      twitter: map['twitter'] != null ? map['twitter'] as String : null,
      instagram: map['instagram'] != null ? map['instagram'] as String : null,
      website: map['website'] != null ? map['website'] as String : null,
      phone: map['phone'] != null ? map['phone'] as String : null,
    );
  }

  factory SocialsModel.fromJson(String source) =>
      SocialsModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
