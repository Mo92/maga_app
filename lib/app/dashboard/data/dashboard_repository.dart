import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:maga_app/app/dashboard/data/models/gastro_model.dart';

/// This Repository handles all json requests that are related to the dashboard
class DashboardRepository {
  DashboardRepository();

  final db = FirebaseFirestore.instance;

  /// fetches all nearby restaurants
  Future<List<GastroModel>> getGastros() async {
    final firestore = await db.collection('places').get();

    return firestore.docs
        .map<GastroModel>((x) => GastroModel.fromMap(x.data()))
        .toList();
  }
}
