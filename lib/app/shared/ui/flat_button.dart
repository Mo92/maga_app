import 'package:flutter/material.dart';

class FlatButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final String text;

  const FlatButton({super.key, this.onPressed, required this.text});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: TextButton(
        style: TextButton.styleFrom(
          elevation: 1,
          foregroundColor: Colors.black,
          backgroundColor: Colors.white,
          fixedSize: Size(MediaQuery.of(context).size.width * 0.9, 56),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
        ),
        onPressed: onPressed,
        child: Text(text),
      ),
    );
  }
}
