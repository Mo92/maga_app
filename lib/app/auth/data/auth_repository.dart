import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;
import 'package:maga_app/app/auth/data/models/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AuthRepository {
  final firebase_auth.FirebaseAuth _firebaseAuth;
  final db = FirebaseFirestore.instance;

  AuthRepository({
    firebase_auth.FirebaseAuth? firebaseAuth,
  }) : _firebaseAuth = firebaseAuth ?? firebase_auth.FirebaseAuth.instance;

  Future<bool> isAuthenticated() async {
    return _firebaseAuth.currentUser != null;
  }

  Future<void> logInWithEmail(String email, String password) async {
    await _firebaseAuth.signInWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

  Future<String?> registerWithEmail(String email, String password) async {
    final creds = await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );

    return creds.user?.uid;
  }

  Future<void> setUserData(String id, UserModel userModel) async {
    await db.collection('user').doc(id).set(userModel.toMap());
  }

  Future<void> resetPassword(String mail) async {
    await _firebaseAuth.sendPasswordResetEmail(email: mail);
  }
}
