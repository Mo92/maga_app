// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';

class UserModel extends Equatable {
  final String mail;
  final String firstName;
  final String lastName;
  final String postalCode;
  final String city;
  final String firstRegistration;

  const UserModel({
    required this.mail,
    required this.firstName,
    required this.lastName,
    required this.postalCode,
    required this.city,
    required this.firstRegistration,
  });

  @override
  List<Object> get props {
    return [
      mail,
      firstName,
      lastName,
      postalCode,
      city,
    ];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'mail': mail,
      'firstName': firstName,
      'lastName': lastName,
      'postalCode': postalCode,
      'city': city,
      'firstRegistration': firstRegistration,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      mail: map['mail'] as String,
      firstName: map['firstName'] as String,
      lastName: map['lastName'] as String,
      postalCode: map['postalCode'] as String,
      city: map['city'] as String,
      firstRegistration: map['firstRegistration'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
