import 'package:equatable/equatable.dart';

abstract class AuthState extends Equatable {
  @override
  List<Object?> get props => [];
}

class AuthenticationInProgrss extends AuthState {}

class Authenticated extends AuthState {
  Authenticated();
}

class Unathenticated extends AuthState {}

class AuthenticationFailed extends Unathenticated {
  AuthenticationFailed({required this.message});

  final String message;
}

class RegistrationFailed extends Unathenticated {
  final String message;

  RegistrationFailed({required this.message});
}

class RegistrationSuccess extends Unathenticated {}
