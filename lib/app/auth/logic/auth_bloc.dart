import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maga_app/app/auth/data/auth_repository.dart';
import 'package:maga_app/app/auth/data/models/user_model.dart';
import 'package:maga_app/app/auth/logic/auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit({required this.authRepository}) : super(Unathenticated());

  final AuthRepository authRepository;

  Future<void> checkAuthState() async {
    if (await authRepository.isAuthenticated()) {
      emit(Authenticated());
    } else {
      emit(Unathenticated());
    }
  }

  void logInWithEmail(String email, String password) async {
    try {
      await authRepository.logInWithEmail(email, password);

      emit(Authenticated());
    } on FirebaseAuthException {
      emit(AuthenticationFailed(
          message: 'Anmeldung fehlgeschlagen, bitte prüfen Sie Ihre Angaben'));
      emit(Unathenticated());
    }
  }

  void registerWithEmail(String firstName, String lastName, String mail,
      String password, String city, String postalCode) async {
    try {
      final id = await authRepository.registerWithEmail(mail, password);

      await authRepository.setUserData(
        id!,
        UserModel(
          firstName: firstName,
          lastName: lastName,
          mail: mail,
          city: city,
          postalCode: postalCode,
          firstRegistration: DateTime.now().toIso8601String(),
        ),
      );

      emit(RegistrationSuccess());
    } on FirebaseAuthException catch (e) {
      emit(RegistrationFailed(message: e.code));
      log(e.message.toString());
    }
  }

  Future<void> resetPassword(String mail) async {
    try {
      await authRepository.resetPassword(mail);
    } catch (e) {
      log(e.toString());
    }
  }
}
