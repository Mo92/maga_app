import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maga_app/app/auth/logic/auth_bloc.dart';
import 'package:maga_app/app/auth/logic/auth_state.dart';
import 'package:maga_app/app/auth/ui/widgets/login_form_field.dart';
import 'package:maga_app/app/shared/ui/primary_button.dart';
import 'package:maga_app/core/validators.dart';
import 'package:maga_app/router.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({super.key});

  @override
  State<RegistrationScreen> createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final formKey = GlobalKey<FormState>();
  late TextEditingController firstNameControl;
  late TextEditingController lastNameControl;
  late TextEditingController mailControl;
  late TextEditingController passwordControl;
  late TextEditingController cityControl;
  late TextEditingController postalCodeControl;
  late bool policiesAccepted;
  late bool privacyPolicyAccepted;
  late bool autoValidate;
  late bool isPasswordObscure;

  @override
  void initState() {
    super.initState();

    firstNameControl = TextEditingController();
    lastNameControl = TextEditingController();
    mailControl = TextEditingController();
    cityControl = TextEditingController();
    postalCodeControl = TextEditingController();
    passwordControl = TextEditingController();
    policiesAccepted = false;
    privacyPolicyAccepted = false;
    autoValidate = false;
    isPasswordObscure = true;
  }

  Widget _buildBody() {
    return Stack(
      children: [
        Positioned.fill(
          child: Stack(
            children: [
              Container(
                height: 240,
                color: Theme.of(context).primaryColor,
              ),
              Positioned(
                top: 86,
                left: 28,
                child: Text(
                  'REGISTRIEREN',
                  style: Theme.of(context).textTheme.displayMedium!.copyWith(
                        color: Colors.black,
                        fontSize: 36,
                        fontWeight: FontWeight.w500,
                      ),
                ),
              ),
            ],
          ),
        ),
        Positioned.fill(
          top: 146,
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
            ),
            child: SingleChildScrollView(
              child: Center(
                child: Column(
                  children: [
                    Form(
                      key: formKey,
                      autovalidateMode: autoValidate
                          ? AutovalidateMode.onUserInteraction
                          : AutovalidateMode.disabled,
                      child: Column(
                        children: [
                          const SizedBox(height: 48),
                          LoginFormField(
                            label: 'Vorname',
                            validator: Validators.required,
                            controller: firstNameControl,
                          ),
                          const SizedBox(height: 16),
                          LoginFormField(
                            label: 'Nachname',
                            validator: Validators.required,
                            controller: lastNameControl,
                          ),
                          const SizedBox(height: 16),
                          LoginFormField(
                            label: 'E-Mail Adresse',
                            controller: mailControl,
                            validator: Validators.isValidMail,
                          ),
                          const SizedBox(height: 16),
                          LoginFormField(
                            label: 'Passwort',
                            controller: passwordControl,
                            isObscure: isPasswordObscure,
                            validator: Validators.passwordRequirements,
                            suffixIcon: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: IconButton(
                                highlightColor: Colors.transparent,
                                tooltip: 'Passwort anzeigen',
                                icon: Icon(
                                  isPasswordObscure
                                      ? Icons.remove_red_eye_rounded
                                      : Icons.remove_red_eye_outlined,
                                ),
                                color: Colors.grey,
                                onPressed: () {
                                  setState(() {
                                    isPasswordObscure = !isPasswordObscure;
                                  });
                                },
                              ),
                            ),
                          ),
                          const SizedBox(height: 16),
                          LoginFormField(
                            label: 'Stadt',
                            controller: cityControl,
                            validator: Validators.required,
                          ),
                          const SizedBox(height: 16),
                          LoginFormField(
                            validator: Validators.isPostalCode,
                            label: 'PLZ',
                            controller: postalCodeControl,
                          ),
                          const SizedBox(height: 16),
                          CheckboxListTile(
                            value: policiesAccepted,
                            // isError: !policiesAccepted && autoValidate,
                            isError: true,
                            controlAffinity: ListTileControlAffinity.leading,
                            title: Text(
                              'Ich stimme den AGB und Nutzungsbedingungen der MAGA App zu',
                              style: !policiesAccepted && autoValidate
                                  ? const TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.w600,
                                    )
                                  : null,
                            ),
                            onChanged: (value) => setState(
                              () {
                                policiesAccepted = !policiesAccepted;
                              },
                            ),
                          ),
                          CheckboxListTile(
                            value: privacyPolicyAccepted,
                            controlAffinity: ListTileControlAffinity.leading,
                            title: Text(
                              'Ich stimme den Datenschutzrichtlinien der MAGA App zu',
                              style: !privacyPolicyAccepted && autoValidate
                                  ? const TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.w600,
                                    )
                                  : null,
                            ),
                            onChanged: (value) => setState(
                              () {
                                privacyPolicyAccepted = !privacyPolicyAccepted;
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 24),
                    PrimaryButton(
                      text: 'Registrieren',
                      onPressed: (privacyPolicyAccepted && policiesAccepted)
                          ? () => register()
                          : null,
                    ),
                    const SizedBox(height: 24),
                    Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text('Bereits registriert?'),
                          GestureDetector(
                            onTap: () => Navigator.of(context)
                                .pushReplacementNamed(MagaRouter.loginRoute),
                            child: Text(
                              'Anmelden',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyLarge!
                                  .copyWith(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary,
                                      fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 24),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void register() {
    setState(() {
      autoValidate = true;
    });

    if (formKey.currentState!.validate()) {
      context.read<AuthCubit>().registerWithEmail(
          firstNameControl.text,
          lastNameControl.text,
          mailControl.text,
          passwordControl.text,
          cityControl.text,
          postalCodeControl.text);
    }
  }

  @override
  void dispose() {
    firstNameControl.dispose();
    lastNameControl.dispose();
    mailControl.dispose();
    cityControl.dispose();
    postalCodeControl.dispose();
    passwordControl.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthCubit, AuthState>(
        listener: (context, state) {
          if (state is RegistrationFailed) {
            ScaffoldMessenger.of(context)
              ..clearSnackBars()
              ..showSnackBar(
                SnackBar(
                  content: Text(
                    state.message,
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .labelLarge
                        ?.copyWith(color: Colors.white),
                  ),
                  backgroundColor: Theme.of(context).colorScheme.error,
                ),
              );
          }
          if (state is RegistrationSuccess) {
            WidgetsBinding.instance.addPostFrameCallback(
              (timeStamp) {
                Navigator.of(context)
                    .pushReplacementNamed(MagaRouter.registrationSuccessRoute);
              },
            );
          }
        },
        child: Scaffold(
          extendBody: true,
          body: _buildBody(),
        ));
  }
}
