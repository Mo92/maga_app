import 'package:flutter/material.dart';

class LoginFormField extends StatefulWidget {
  final TextEditingController? controller;
  final String? label;
  final FormFieldValidator<String>? validator;
  final bool? isObscure;
  final Widget? suffixIcon;

  const LoginFormField({
    super.key,
    this.controller,
    this.label,
    this.validator,
    this.isObscure,
    this.suffixIcon,
  });

  @override
  State<LoginFormField> createState() => _LoginFormFieldState();
}

class _LoginFormFieldState extends State<LoginFormField> {
  late TextEditingController inputController;
  @override
  void initState() {
    inputController = widget.controller ?? TextEditingController();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: TextFormField(
        controller: inputController,
        validator: widget.validator,
        cursorColor: Theme.of(context).colorScheme.secondary,
        obscureText: widget.isObscure ?? false,
        decoration: InputDecoration(
          prefix: const SizedBox(width: 8),
          suffixIcon: widget.suffixIcon,
          floatingLabelBehavior: FloatingLabelBehavior.never,
          filled: true,
          hintText: widget.label,
          hoverColor: Colors.black,
          fillColor: Colors.grey[200],
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(30),
          ),
        ),
      ),
    );
  }
}
