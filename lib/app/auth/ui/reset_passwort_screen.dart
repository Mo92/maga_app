import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maga_app/app/auth/ui/widgets/login_form_field.dart';
import 'package:maga_app/app/shared/ui/primary_button.dart';
import 'package:maga_app/core/validators.dart';
import 'package:maga_app/router.dart';

import '../logic/auth_bloc.dart';

class ResetPasswordScreen extends StatefulWidget {
  const ResetPasswordScreen({super.key});

  @override
  State<ResetPasswordScreen> createState() => _RegistrationSuccessScreenState();
}

class _RegistrationSuccessScreenState extends State<ResetPasswordScreen> {
  final GlobalKey<FormState> formKey = GlobalKey();

  late TextEditingController mailControl;
  late bool autoValidate;

  @override
  void initState() {
    mailControl = TextEditingController();
    autoValidate = false;

    super.initState();
  }

  _buildBody(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: Stack(
            children: [
              Container(
                height: 400,
                color: Theme.of(context).primaryColor,
              ),
              Positioned(
                top: 86,
                left: 28,
                child: Text(
                  'PASSWORT \nZURÜCKSETZEN',
                  overflow: TextOverflow.fade,
                  style: Theme.of(context).textTheme.displayMedium!.copyWith(
                        color: Colors.black,
                        fontSize: 36,
                        fontWeight: FontWeight.w500,
                      ),
                ),
              ),
            ],
          ),
        ),
        Positioned.fill(
          top: 360,
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
            ),
            child: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(height: 12),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Text(
                        'Falls du dein Passwort vergessen hast, kannst du hier deine E-Mail-Adresse eingeben. Wenn deine E-Mail im System hinterlegt ist, senden wir dir eine Anleitung zum Zurücksetzen deines Passworts per E-Mail zu.',
                        style: Theme.of(context).textTheme.headlineSmall,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  LoginFormField(
                    controller: mailControl,
                    validator: Validators.isValidMail,
                    label: 'E-Mail Adresse',
                  ),
                  PrimaryButton(
                    text: 'Senden',
                    onPressed: () => submit(),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.of(context)
                        .pushReplacementNamed(MagaRouter.loginRoute),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text('Zurück zur '),
                          Text(
                            'Anmeldung',
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge!
                                .copyWith(
                                    color:
                                        Theme.of(context).colorScheme.secondary,
                                    fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 12),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  submit() {
    setState(() {
      autoValidate = true;
    });

    if (formKey.currentState!.validate()) {
      context.read<AuthCubit>().resetPassword(mailControl.text);

      ScaffoldMessenger.of(context)
        ..clearSnackBars()
        ..showSnackBar(
          SnackBar(
            duration: const Duration(seconds: 8),
            content: Text(
              'Vielen Dank! Deine Anfrage wurde erfolgreich übermittelt. Wenn die eingegebene E-Mail-Adresse in unserem System vorhanden ist, erhältst du in Kürze eine Nachricht von uns.',
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .labelLarge
                  ?.copyWith(color: Colors.white),
            ),
            backgroundColor: Colors.green[800],
          ),
        );

      WidgetsBinding.instance.addPostFrameCallback(
        (_) {
          Navigator.of(context).pushReplacementNamed(MagaRouter.loginRoute);
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: _buildBody(context),
    );
  }
}
