import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maga_app/app/auth/logic/auth_bloc.dart';
import 'package:maga_app/app/auth/logic/auth_state.dart';
import 'package:maga_app/app/auth/ui/widgets/login_form_field.dart';
import 'package:maga_app/app/shared/ui/flat_button.dart';
import 'package:maga_app/app/shared/ui/primary_button.dart';
import 'package:maga_app/core/validators.dart';
import 'package:maga_app/router.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late bool isPasswordObscure;
  late TextEditingController mailController;
  late TextEditingController passwordController;
  late bool autoValidate;
  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    mailController = TextEditingController();
    passwordController = TextEditingController();
    isPasswordObscure = true;
    autoValidate = false;

    super.initState();
  }

  Widget _buildLoginBody(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: Stack(
            children: [
              Container(
                height: 260,
                color: Theme.of(context).primaryColor,
              ),
              Positioned(
                top: 86,
                left: 28,
                child: Text(
                  'ANMELDEN',
                  style: Theme.of(context).textTheme.displayMedium!.copyWith(
                        color: Colors.black,
                        fontSize: 36,
                        fontWeight: FontWeight.w500,
                      ),
                ),
              ),
            ],
          ),
        ),
        Positioned.fill(
          top: 248,
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                const SizedBox(height: 84),
                Form(
                  autovalidateMode: autoValidate
                      ? AutovalidateMode.onUserInteraction
                      : AutovalidateMode.disabled,
                  key: formKey,
                  child: Column(
                    children: [
                      LoginFormField(
                        validator: Validators.isValidMail,
                        controller: mailController,
                        label: 'E-Mail',
                      ),
                      const SizedBox(height: 16),
                      LoginFormField(
                        label: 'Passwort',
                        controller: passwordController,
                        isObscure: isPasswordObscure,
                        validator: Validators.required,
                        suffixIcon: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: IconButton(
                            highlightColor: Colors.transparent,
                            tooltip: 'Passwort anzeigen',
                            icon: Icon(
                              isPasswordObscure
                                  ? Icons.remove_red_eye_rounded
                                  : Icons.remove_red_eye_outlined,
                            ),
                            color: Colors.grey,
                            onPressed: () {
                              setState(() {
                                isPasswordObscure = !isPasswordObscure;
                              });
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 32),
                Align(
                  alignment: Alignment.centerRight,
                  child: PrimaryButton(
                    text: 'Anmelden',
                    onPressed: () => login(),
                  ),
                ),
                const SizedBox(height: 8),
                TextButton(
                  onPressed: () => Navigator.of(context)
                      .pushReplacementNamed(MagaRouter.resetPasswordRoute),
                  child: Text('Passwort vergessen ?',
                      style: Theme.of(context).textTheme.bodyLarge),
                ),
                const SizedBox(height: 38),
                FlatButton(
                  text: 'Mit Apple anmelden',
                  onPressed: () => log('apple'),
                ),
                const SizedBox(height: 12),
                FlatButton(
                  text: 'Weiter mit Google',
                  onPressed: () => log('google'),
                ),
                const SizedBox(height: 52),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    children: [
                      const Text('Du hast noch keinen Account?'),
                      const SizedBox(height: 8),
                      GestureDetector(
                        onTap: () => Navigator.of(context)
                            .pushReplacementNamed(MagaRouter.registrationRoute),
                        child: Text(
                          'Registrieren',
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge!
                              .copyWith(
                                  color:
                                      Theme.of(context).colorScheme.secondary,
                                  fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  void login() {
    setState(() {
      autoValidate = true;
    });

    if (formKey.currentState!.validate()) {
      context
          .read<AuthCubit>()
          .logInWithEmail(mailController.text, passwordController.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is AuthenticationFailed) {
          ScaffoldMessenger.of(context)
            ..clearSnackBars()
            ..showSnackBar(
              SnackBar(
                content: Text(
                  state.message,
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .labelLarge
                      ?.copyWith(color: Colors.white),
                ),
                backgroundColor: Theme.of(context).colorScheme.error,
              ),
            );
        }
      },
      child: BlocBuilder<AuthCubit, AuthState>(
        builder: (context, state) {
          if (state is Unathenticated) {
            return Scaffold(
              extendBody: true,
              body: _buildLoginBody(context),
            );
          }

          if (state is Authenticated) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              Navigator.of(context)
                  .pushReplacementNamed(MagaRouter.dashboardRoute);
            });
          }

          return Container();
        },
      ),
    );
  }
}
