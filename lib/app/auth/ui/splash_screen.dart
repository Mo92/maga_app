import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maga_app/app/auth/logic/auth_bloc.dart';
import 'package:maga_app/app/auth/logic/auth_state.dart';
import 'package:maga_app/app/shared/ui/flat_button.dart';
import 'package:maga_app/app/shared/ui/primary_button.dart';
import 'package:maga_app/router.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthCubit, AuthState>(
      builder: (context, state) {
        context.read<AuthCubit>().checkAuthState();

        if (state is Authenticated) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.of(context)
                .pushReplacementNamed(MagaRouter.dashboardRoute);
          });
          return Container();
        }

        return Scaffold(
          body: SafeArea(
            child: SizedBox(
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 36),
                        Text(
                          'WILLKOMMEN',
                          style: Theme.of(context)
                              .textTheme
                              .displayMedium!
                              .copyWith(
                                color: Colors.black,
                                fontSize: 36,
                                fontWeight: FontWeight.w500,
                              ),
                        ),
                        Text(
                          'Lorem Dolar Sit Amet',
                          textAlign: TextAlign.start,
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        PrimaryButton(
                          text: 'Anmelden',
                          onPressed: () => Navigator.of(context)
                              .pushReplacementNamed(MagaRouter.loginRoute),
                        ),
                        const SizedBox(height: 24),
                        FlatButton(
                          text: 'Registrieren',
                          onPressed: () =>
                              Navigator.of(context).pushReplacementNamed(
                            MagaRouter.registrationRoute,
                          ),
                        ),
                        const SizedBox(height: 56),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
