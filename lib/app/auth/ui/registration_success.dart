import 'package:flutter/material.dart';
import 'package:maga_app/app/shared/ui/primary_button.dart';
import 'package:maga_app/router.dart';

class RegistrationSuccessScreen extends StatelessWidget {
  const RegistrationSuccessScreen({super.key});

  _buildBody(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: Stack(
            children: [
              Container(
                height: 400,
                color: Theme.of(context).primaryColor,
              ),
              Positioned(
                top: 86,
                left: 28,
                child: Text(
                  'FERTIG',
                  style: Theme.of(context).textTheme.displayMedium!.copyWith(
                        color: Colors.black,
                        fontSize: 36,
                        fontWeight: FontWeight.w500,
                      ),
                ),
              ),
            ],
          ),
        ),
        Positioned.fill(
          top: 360,
          child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  const SizedBox(height: 12),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Text(
                        'Registrierung erfolgreich! Wir haben dir eine Bestätigungsmail geschickt. Bitte bestätige deine E-Mail-Adresse, um die App sofort zu nutzen.',
                        style: Theme.of(context).textTheme.headlineSmall,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  PrimaryButton(
                    text: 'Zur Anmeldung',
                    onPressed: () => Navigator.of(context)
                        .pushReplacementNamed(MagaRouter.loginRoute),
                  ),
                  const SizedBox(height: 12),
                ],
              )),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: _buildBody(context),
    );
  }
}
