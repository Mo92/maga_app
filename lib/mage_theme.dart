import 'package:flutter/material.dart';

const MaterialColor primaryColor =
    MaterialColor(_primaryColorValue, <int, Color>{
  50: Color(0xFFFEFAF1),
  100: Color(0xFFFCF3DD),
  200: Color(0xFFFAECC6),
  300: Color(0xFFF7E4AF),
  400: Color(0xFFF6DE9E),
  500: Color(_primaryColorValue),
  600: Color(0xFFF3D485),
  700: Color(0xFFF1CE7A),
  800: Color(0xFFEFC870),
  900: Color(0xFFECBF5D),
});
const int _primaryColorValue = 0xFFF4D88D;

final magaLightTheme = ThemeData(
  primaryColor: primaryColor,
  colorScheme: ColorScheme.fromSwatch(
    primarySwatch: primaryColor,
    backgroundColor: const Color(0xFFFFFFF1),
    accentColor: const Color(0xFF8EA5F5),
  ),
);

extension MagaTextTheme on TextTheme {
  TextStyle get regularInfo => const TextStyle();
  TextStyle get title => const TextStyle(fontSize: 24);
  TextStyle get info =>
      const TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
}
